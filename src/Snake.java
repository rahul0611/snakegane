import java.applet.AudioClip;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.border.Border;


public class Snake extends JFrame {

    /**
	 * 
	 */
	 AudioClip soundFile1;
	 static JCheckBox sound = new JCheckBox("Sound"); // check box to turn the
	    
	private static final long serialVersionUID = 1L;
	 private static java.applet.AudioClip music = null; // audio clip music

	    // sound method that initializes the music variable
	    public void sound() {
	        try {
	            if (music == null) {
	                URL backgroundMusicURL = this.getClass().getResource(
	                        "trippygaia1.mid");
	                music = java.applet.Applet.newAudioClip(backgroundMusicURL);
	            }
	        } catch (Exception e1) {
	            System.out.println("Audio not found");
	        }
	    }
	public Snake() {

        add(new Board());

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(470, 360);
        setLocationRelativeTo(null);
        setTitle("Snake");
        JToolBar toolBar;
		toolBar = new JToolBar("Game");

		// adding play game and help buttons with their respective icons to tool
		// bar with theirs listeners
		JButton game = new JButton("Play Game....", new ImageIcon("start.gif"));
		JButton help = new JButton("Help....", new ImageIcon("help.gif"));
		help.addActionListener(new ActionListener() {
			// Gives the instructions to play game
			public void actionPerformed(ActionEvent e) {
				String string = "1. Increase the length of your snake by eating the apples\n"
						+ "2. It shouldnt hit walls and stones if incase fialed not protect the snake from walls and stones then the game ends";
				JOptionPane.showMessageDialog(null, string);
			}
		});

		
		
		// adding the buttons to tool bar
		game.setFocusable(false);
		help.setFocusable(false);
		toolBar.add(game);
		toolBar.add(help);
		toolBar.setFocusable(false);
		JMenuBar menubar = new JMenuBar();

        // Game menu
        JMenu gameMenu = new JMenu("Game");
        // setting its mnemonic
        gameMenu.setMnemonic(KeyEvent.VK_G);

        // menu item
        JMenuItem newGame = new JMenuItem("New");
        newGame.setMnemonic(KeyEvent.VK_N);
        // adding action listener to the menu item
        newGame.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                //gameBoard.redraw();
            }
        });

        // exit button for the game menu with its mnemonic and action listener
        JMenuItem exitGame = new JMenuItem("Exit");
        exitGame.setMnemonic(KeyEvent.VK_X);
        exitGame.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        // adding the menu items to the menu
        gameMenu.add(newGame);
        gameMenu.add(exitGame);

        // Help menu with its items,mnemonics and listeners
        JMenu helpMenu = new JMenu("Help");
        helpMenu.setMnemonic(KeyEvent.VK_H);
        JMenuItem rules = new JMenuItem("Rules");
        rules.setMnemonic(KeyEvent.VK_R);
        rules.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                String string = "1. The snake should eat as many apples as possible\n"
                        + "2. It shouldnt hit walls";
                JOptionPane.showMessageDialog(null, string);
            }
        });

        JMenuItem aboutGame = new JMenuItem("About");
        aboutGame.setMnemonic(KeyEvent.VK_A);
        aboutGame.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                String string = "Created in 2013...by Nikhil & Sahithi";
                JOptionPane.showMessageDialog(null, string);
            }
        });

        // slider
        // JSlider speedController = new JSlider();
        // JLabel controller = new JLabel("Slide to control the speed....!!! ");
        // adding the items to the help menu
        helpMenu.add(rules);
        helpMenu.add(aboutGame);
        // south panel
        JPanel south = new JPanel();
     // Sound option
        south.add(sound);

        // listener class for the sound check box to stop and play the music
        // with selection change
        class chBox implements ItemListener {

            public void itemStateChanged(ItemEvent e) {
                if (sound.isSelected()) {
                    music.play();
                } else {
                    music.stop();
                }

            }
        }// end of checkBox class
        sound.addItemListener(new chBox());

        // Adding to menu bar
        menubar.add(gameMenu);
        menubar.add(helpMenu);
        // menubar.add(speedController);
        // menubar.add(controller);

		
		 JPanel north = new JPanel();
	        north.setLayout(new BorderLayout());
	        north.add(toolBar,BorderLayout.SOUTH);
	        north.add(menubar,BorderLayout.NORTH);
	        add(north,BorderLayout.NORTH);
	        add(south,BorderLayout.SOUTH);
		setResizable(false);
        
        
        
        setVisible(true);
    }

    public static void main(String[] args) {
        new Snake();
    }
}
