import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;


public class Board extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	private final int WIDTH 	= 470;
    private final int HEIGHT 	= 360;
    private final int DOT_SIZE 	= 15;
    private final int ALL_DOTS 	= 600;
    private final int DELAY 	= 140;

    private int 	  x[] 		= new int[ALL_DOTS];
    private int       y[] 		= new int[ALL_DOTS];

    private int 	  dots;
    private int 	  apple_x[]	= new int[10]; //array of apples'x coordinates
    private int 	  apple_y[] = new int[10]; //array of apples'y coordinates

    private boolean   left 		= false;
    private boolean   right 	= true;
    private boolean   up 		= false;
    private boolean   down 		= false;
    private boolean   inGame 	= true;

    private Timer 	  timer;
    private Image 	  ball;
    private Image 	  apple;
    private Image 	  head;

    public Board() 
    {
    	addKeyListener(new TAdapter());
        
        ImageIcon iid = new ImageIcon(this.getClass().getResource("snakebody.gif"));
        ball = iid.getImage();

        ImageIcon iia = new ImageIcon(this.getClass().getResource("apple.gif"));
        apple = iia.getImage();

        ImageIcon iih = new ImageIcon(this.getClass().getResource("snakehead.gif"));
        head = iih.getImage();

        setFocusable(true);
        initGame();
    }

    public void initGame() 
    {
        dots = 3;

        for (int z = 0; z < dots; z++) 
        {
        	x[z] = 45 - z*15;
            y[z] = 15;
        }

        locateApple();
        timer = new Timer(DELAY, this);
        timer.start();
    }


    public void paint(Graphics g) 
    {
        super.paint(g);
        if (inGame) 
        {
        	for(int z=0;z<10;z++)
                g.drawImage(apple, apple_x[z], apple_y[z], this);

            for (int z = 0; z < dots; z++) {
                if (z == 0)
                    g.drawImage(head, x[z], y[z], this);
                else g.drawImage(ball, x[z], y[z], this);
            }

            Toolkit.getDefaultToolkit().sync();
            g.dispose();

        } else {
            gameOver(g);
        }
    }


    public void gameOver(Graphics g) {
        String msg = "Game Over";
        Font small = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metr = this.getFontMetrics(small);

        g.setColor(Color.white);
        g.setFont(small);
        g.drawString(msg, (WIDTH - metr.stringWidth(msg)) / 2,
                     HEIGHT / 2);
    }


    public void checkApple() {

    	for(int z=0;z<10;z++)
    	{
        if ((x[0] == apple_x[z]) && (y[0] == apple_y[z])) {
            dots++;
            locateApple();
        }
    	}
    }


    public void move() {

        for (int z = dots; z > 0; z--) {
            x[z] = x[(z - 1)];
            y[z] = y[(z - 1)];
        }

        if (left) {
            x[0] -= DOT_SIZE;
        }

        if (right) {
            x[0] += DOT_SIZE;
        }

        if (up) {
            y[0] -= DOT_SIZE;
        }

        if (down) {
            y[0] += DOT_SIZE;
        }
    }


    public void checkCollision() {

          for (int z = dots; z > 0; z--) {

              if ((z > 4) && (x[0] == x[z]) && (y[0] == y[z])) {
                  inGame = false;
              }
          }

        if (y[0] > HEIGHT) {
            inGame = false;
        }

        if (y[0] < 0) {
            inGame = false;
        }

        if (x[0] > WIDTH) {
            inGame = false;
        }

        if (x[0] < 0) {
            inGame = false;
        }
    }

    public void locateApple() 
	{
		for(int z=0;z<10;z++)
			{
				apple_x[z] = (15*(int)(Math.random()*30));
				apple_y[z] = (15*(int)(Math.random()*15));
			}
	}

    public void actionPerformed(ActionEvent e) {

        if (inGame) {
            checkApple();
            checkCollision();
            move();
        }

        repaint();
    }


    private class TAdapter extends KeyAdapter {

        public void keyPressed(KeyEvent e) {

            int key = e.getKeyCode();

            if ((key == KeyEvent.VK_LEFT) && (!right)) {
                left = true;
                up = false;
                down = false;
            }

            if ((key == KeyEvent.VK_RIGHT) && (!left)) {
                right = true;
                up = false;
                down = false;
            }

            if ((key == KeyEvent.VK_UP) && (!down)) {
                up = true;
                right = false;
                left = false;
            }

            if ((key == KeyEvent.VK_DOWN) && (!up)) {
                down = true;
                right = false;
                left = false;
            }
        }
    }
}
